import React from 'react';
import * as PIXI from "pixi.js";
import IPlacedTile from '../interfaces/PlacedTile';
import FloorGrate from './tiles/FloorGrate';
import BaseTile from './tiles/BaseTile';
import Floor from './tiles/Floor';
import Floor2 from './tiles/Floor2';
import Floor3 from './tiles/Floor2';

export default class Stage extends React.Component<any, any> {
    private app: PIXI.Application;
    private tiles:IPlacedTile[] = [];
    private TILE_SIZE:number = 128;
    private TILE_OFFSET:number = this.TILE_SIZE/2;

    constructor(props: any) {
        super(props);
        this.app = new PIXI.Application({
            width: window.innerWidth,
            height: window.innerHeight,
            backgroundColor: 0x000000,
            // resolution: window.devicePixelRatio || 1
            resolution: 1
        });

        this.addTile(this.getRandomTile(), 0, 0);
        this.addTile(this.getRandomTile(), 0, 1);
        this.addTile(this.getRandomTile(), 0, 2);

        this.addTile(this.getRandomTile(), 1, 0);
        this.addTile(this.getRandomTile(), 1, 1);
        this.addTile(this.getRandomTile(), 1, 2);

        this.addTile(this.getRandomTile(), 2, 0);
        this.addTile(this.getRandomTile(), 2, 1);
        this.addTile(this.getRandomTile(), 2, 2);

        this.addTile(this.getRandomTile(), 3, 0);
        this.addTile(this.getRandomTile(), 3, 1);
        this.addTile(this.getRandomTile(), 3, 2);

        this.addTile(this.getRandomTile(), 4, 0);
        this.addTile(this.getRandomTile(), 4, 1);
        this.addTile(this.getRandomTile(), 4, 2);

        this.addTile(this.getRandomTile(), 5, 0);
        this.addTile(this.getRandomTile(), 5, 1);
        this.addTile(this.getRandomTile(), 5, 2);


        this.addTile(this.getRandomTile(), 0, 3);
        this.addTile(this.getRandomTile(), 0, 4);
        this.addTile(this.getRandomTile(), 0, 5);

        this.addTile(this.getRandomTile(), 1, 3);
        this.addTile(this.getRandomTile(), 1, 4);
        this.addTile(this.getRandomTile(), 1, 5);

        this.addTile(this.getRandomTile(), 2, 3);
        this.addTile(this.getRandomTile(), 2, 4);
        this.addTile(this.getRandomTile(), 2, 5);

        this.addTile(this.getRandomTile(), 3, 3);
        this.addTile(this.getRandomTile(), 3, 4);
        this.addTile(this.getRandomTile(), 3, 5);

        this.addTile(this.getRandomTile(), 4, 3);
        this.addTile(this.getRandomTile(), 4, 4);
        this.addTile(this.getRandomTile(), 4, 5);

        this.addTile(this.getRandomTile(), 5, 3);
        this.addTile(this.getRandomTile(), 5, 4);
        this.addTile(this.getRandomTile(), 5, 5);
    }

    private addTile(tile:BaseTile, x:number, y:number) {
        this.tiles.push({
            tile,
            x,
            y
        });

        tile.setPosition(this.TILE_OFFSET + this.TILE_SIZE*x, this.TILE_OFFSET+this.TILE_SIZE*y);
        this.app.stage.addChild(tile);
    }

    private getRandomTile() {
        let tiles = [Floor, Floor2, Floor3, FloorGrate];
        switch(tiles[Math.floor(Math.random() * tiles.length)]) {
            case Floor:
                return new Floor();
            case Floor2:
                return new Floor2();
            case Floor3:
                return new Floor3();
            default:
                return new FloorGrate();
        }
    }

    public render() {
        return (
            <div id="stage" />
        );
    }

    public componentDidMount() {
        document.getElementById("stage")?.appendChild(this.app.view);
    }
}