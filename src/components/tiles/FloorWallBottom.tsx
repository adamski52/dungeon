import BaseTile from "./BaseTile";
import img from "../../img/floor-wall-b.png";
import { TileTypes } from "../../enums/TileTypes";
import Floor from "./Floor";
import Floor2 from "./Floor2";
import Floor3 from "./Floor3";
import FloorGrate from "./FloorGrate";
import { Directions } from "../../enums/Directions";

export default class FloorWallBottom extends BaseTile {
    constructor() {
        super({
            img,
            type: TileTypes.FLOOR_WITH_WALL,
            allowedNeighbors: {
                [Directions.NORTH]: [Floor, Floor2, Floor3, FloorGrate],
                [Directions.EAST]: [Floor, Floor2, Floor3, FloorGrate],
                [Directions.SOUTH]: [Floor, Floor2, Floor3, FloorGrate],
                [Directions.WEST]: [Floor, Floor2, Floor3, FloorGrate]
            }
        });
    }
}
