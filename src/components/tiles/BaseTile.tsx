import INeighbors from "../../interfaces/Neighbors";
import ITileProps from "../../interfaces/TileProps";
import {Directions} from "../../enums/Directions";
import * as PIXI from "pixi.js";
import IAllowedNeighbors from "../../interfaces/AllowedNeighbors";

export default class BaseTile extends PIXI.Container {
    private image:PIXI.Texture;
    private sprite:PIXI.Sprite;

    private allowedNeighbors:IAllowedNeighbors = {
        [Directions.NORTH]: [undefined],
        [Directions.EAST]: [undefined],
        [Directions.SOUTH]: [undefined],
        [Directions.WEST]: [undefined]
    };

    private neighbors:INeighbors = {
        [Directions.NORTH]: undefined,
        [Directions.EAST]: undefined,
        [Directions.SOUTH]: undefined,
        [Directions.WEST]: undefined
    };

    constructor(props:ITileProps) {
        super();

        if(props.allowedNeighbors) {
            this.allowedNeighbors = props.allowedNeighbors;
        }

        this.image = PIXI.Texture.from(props.img);
        this.sprite = new PIXI.Sprite(this.image);

        this.sprite.anchor.x = .5;
        this.sprite.anchor.y = .5;

        this.sprite.setTransform(undefined, undefined, undefined, undefined, this.getRandomRotation());

        this.addChild(this.sprite);
    }

    private getRandomRotation() {
        let rotations = [0, Math.PI * .5, Math.PI, Math.PI * 1.5];
        return rotations[Math.floor(Math.random() * rotations.length)];
    }

    public setNeighbor(tile:BaseTile, direction:Directions) {
        this.neighbors[direction] = tile;
    }

    public setPosition(x:number, y:number) {
        this.x = x;
        this.y = y;
    }
}
