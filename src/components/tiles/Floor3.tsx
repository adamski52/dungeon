import BaseTile from "./BaseTile";
import img from "../../img/floor3.png";
import { TileTypes } from "../../enums/TileTypes";
import { Directions } from "../../enums/Directions";
import Floor from "./Floor";
import Floor2 from "./Floor2";
import FloorGrate from "./FloorGrate";
import FloorWallBottom from "./FloorWallBottom";

export default class Floor3 extends BaseTile {
    constructor() {
        super({
            img,
            type: TileTypes.FLOOR,
            allowedNeighbors: {
                [Directions.NORTH]: [Floor, Floor2, Floor3, FloorGrate],
                [Directions.EAST]: [Floor, Floor2, Floor3, FloorGrate],
                [Directions.SOUTH]: [Floor, Floor2, Floor3, FloorGrate, FloorWallBottom],
                [Directions.WEST]: [Floor, Floor2, Floor3, FloorGrate]
            }
        });
    }
}
