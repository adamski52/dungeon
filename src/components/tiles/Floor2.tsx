import BaseTile from "./BaseTile";
import img from "../../img/floor2.png";
import { TileTypes } from "../../enums/TileTypes";
import FloorWallBottom from "./FloorWallBottom";
import FloorGrate from "./FloorGrate";
import Floor from "./Floor";
import Floor3 from "./Floor3";
import { Directions } from "../../enums/Directions";

export default class Floor2 extends BaseTile {
    constructor() {
        super({
            img,
            type: TileTypes.FLOOR,
            allowedNeighbors: {
                [Directions.NORTH]: [Floor, Floor2, Floor3, FloorGrate],
                [Directions.EAST]: [Floor, Floor2, Floor3, FloorGrate],
                [Directions.SOUTH]: [Floor, Floor2, Floor3, FloorGrate, FloorWallBottom],
                [Directions.WEST]: [Floor, Floor2, Floor3, FloorGrate]
            }
        });
    }
}
