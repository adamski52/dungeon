import BaseTile from "../components/tiles/BaseTile";

export default interface IPlacedTile {
    tile: BaseTile;
    x: number;
    y: number;
}