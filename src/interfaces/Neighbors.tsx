import BaseTile from "../components/tiles/BaseTile";
import { Directions } from "../enums/Directions";

export default interface INeighbors {
    [Directions.NORTH]: BaseTile | undefined;
    [Directions.EAST]: BaseTile | undefined;
    [Directions.SOUTH]: BaseTile | undefined;
    [Directions.WEST]: BaseTile | undefined;
}
