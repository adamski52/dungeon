import BaseTile from "../components/tiles/BaseTile";
import { Directions } from "../enums/Directions";

export default interface IAllowedNeighbors {
    [Directions.NORTH]: (typeof BaseTile | undefined)[];
    [Directions.EAST]: (typeof BaseTile | undefined)[];
    [Directions.SOUTH]: (typeof BaseTile | undefined)[];
    [Directions.WEST]: (typeof BaseTile | undefined)[];
}
