import { TileTypes } from "../enums/TileTypes";
import IAllowedNeighbors from "./AllowedNeighbors";

export default interface ITileProps {
    img: string;
    type: TileTypes;
    allowedNeighbors?: IAllowedNeighbors;
}
