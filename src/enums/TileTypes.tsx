export enum TileTypes {
    FLOOR,
    FLOOR_WITH_WALL,
    WALL,
    WATER
}
